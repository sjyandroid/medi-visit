package test.app.medicalrep;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Handler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.app.medicalrep.activity.AddInstitutionActivity;
import test.app.medicalrep.activity.EmployeeListActivity;
import test.app.medicalrep.activity.HospitalActivity;
import test.app.medicalrep.activity.SearchInstitutionActivity;
import test.app.medicalrep.model.Institution;
import test.app.medicalrep.model.InstitutionResponse;
import test.app.medicalrep.rest.ApiClient;
import test.app.medicalrep.rest.ApiInterface;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LocationListener, OnMapReadyCallback, ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    GoogleApiClient mGoogleApiClient;
    FloatingActionButton mFabSearch;
    FloatingActionButton mFabAddInstitution;
    public static String CURRENT_LOCATION_LAT = "";
    public static String CURRENT_LOCATION_LNG = "";

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFabSearch = (FloatingActionButton) findViewById(R.id.fabSearch);
        mFabAddInstitution = (FloatingActionButton) findViewById(R.id.fabAddInstitution);

        mFabSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SearchInstitutionActivity.class);
                intent.putExtra("institutions", mArrayInstitutionList);
                startActivity(intent);
            }
        });

        mFabAddInstitution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AddInstitutionActivity.class));
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        //if (statusOfGPS) {
        getLocation();

        /*if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();



            }*/
        /*} else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Please turn ON your location service to continue");
            dialog.setCancelable(false);
            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            dialog.show();

        }*/


    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();

    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        1000).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    ArrayList<Institution> mArrayInstitutionList;

    void getInstitutions() {

        final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Please Wait");
        dialog.setCancelable(false);
        dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<InstitutionResponse> call = apiService.getInstitutions(
                getSharedPreferences("app_prefs", MODE_PRIVATE).getString("userID", ""));
        call.enqueue(new Callback<InstitutionResponse>() {
            @Override
            public void onResponse(Call<InstitutionResponse> call, Response<InstitutionResponse> response) {
                dialog.dismiss();
                if (response.body() != null) {
                    mArrayInstitutionList = response.body().getInstitutions();
                    plotInstitutions(true);
                } else {
                    Toast.makeText(MainActivity.this, "UNAUTHORIZED USER", Toast.LENGTH_LONG).show();
                    /*getSharedPreferences("app_prefs", MODE_PRIVATE).edit().clear().commit();
                    startActivity(new Intent(MainActivity.this, SplashActivity.class));
                    finish();*/
                }
            }

            @Override
            public void onFailure(Call<InstitutionResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(MainActivity.this, "Unknown error. Please try after some time", Toast.LENGTH_LONG).show();
            }
        });

    }


    HashMap<String, Integer> mMarkers = new HashMap<String, Integer>();

    void plotInstitutions(boolean isCurrentLocation) {
        mGoogleMap.clear();
        mMarkers.clear();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }



        //Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        double longitude;
        double latitude;
        if(isCurrentLocation) {
            longitude = Double.parseDouble(CURRENT_LOCATION_LNG);
            latitude = Double.parseDouble(CURRENT_LOCATION_LAT);
        } else {
            longitude = Double.parseDouble(changedLatitude);
            latitude = Double.parseDouble(changedLongitude);
        }

        Location startPoint = new Location("locationA");
        startPoint.setLatitude(latitude);
        startPoint.setLongitude(longitude);

        for (int i = 0; i < mArrayInstitutionList.size(); i++) {

            Location endPoint = new Location("locationB");
            endPoint.setLatitude(Double.parseDouble(mArrayInstitutionList.get(i).getInstLat()));
            endPoint.setLongitude(Double.parseDouble(mArrayInstitutionList.get(i).getInstLng()));

            double distance = startPoint.distanceTo(endPoint);

            if (distance > 6000)
                continue;

            MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(
                    Double.parseDouble(mArrayInstitutionList.get(i).getInstLat()),
                    Double.parseDouble(mArrayInstitutionList.get(i).getInstLng()))).title(
                    mArrayInstitutionList.get(i).getInstName()).icon(
                    BitmapDescriptorFactory.fromResource(R.drawable.hospital_marker));

            /*if(i == 0)
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(Double.parseDouble(mArrayInstitutionList.get(i).getInstLat()),
                        Double.parseDouble(mArrayInstitutionList.get(i).getInstLng())), 12));
*/
            Marker marker = mGoogleMap.addMarker(markerOptions);
            mMarkers.put(marker.getId(), i);

        }

    }

    GoogleMap mGoogleMap;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        mGoogleMap = googleMap;
        //getInstitutions();

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (statusOfGPS) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        3);
                return;
            }

        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Please turn ON your location service to continue");
            dialog.setCancelable(false);
            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            dialog.show();
        }

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent intent = new Intent(MainActivity.this, HospitalActivity.class);
                intent.putExtra("inst_contact",
                        mArrayInstitutionList.get(mMarkers.get(marker.getId())).getInstContact());
                intent.putExtra("inst_addr",
                        mArrayInstitutionList.get(mMarkers.get(marker.getId())).getInstAddr());
                intent.putExtra("inst_offer",
                        mArrayInstitutionList.get(mMarkers.get(marker.getId())).getInstOffer());
                intent.putExtra("inst_type",
                        mArrayInstitutionList.get(mMarkers.get(marker.getId())).getInstType());
                intent.putExtra("inst_lng",
                        mArrayInstitutionList.get(mMarkers.get(marker.getId())).getInstLng());
                intent.putExtra("inst_lat",
                        mArrayInstitutionList.get(mMarkers.get(marker.getId())).getInstLat());
                intent.putExtra("inst_name",
                        mArrayInstitutionList.get(mMarkers.get(marker.getId())).getInstName());
                intent.putExtra("_id",
                        mArrayInstitutionList.get(mMarkers.get(marker.getId())).getId());
                intent.putExtra("__v",
                        mArrayInstitutionList.get(mMarkers.get(marker.getId())).getV());

                startActivity(intent);
            }
        });

        /*mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                System.out.println("onCameraIdle");
                if(mArrayInstitutionList != null && mArrayInstitutionList.size() > 0) {
                    changedLatitude = mGoogleMap.getCameraPosition().target.latitude + "";
                    changedLongitude = mGoogleMap.getCameraPosition().target.longitude + "";
                    plotInstitutions(false);
                }
            }
        });*/

    }

    String changedLatitude;
    String changedLongitude;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case 1:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        mGoogleApiClient.connect();
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        //settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 3: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mGoogleMap != null)
                                getInstitutions();
                        }
                    }, 3000);


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    LocationRequest locationRequest;
    FusedLocationProviderApi fusedLocationProviderApi;

    //GoogleApiClient googleApiClient;
    private void getLocation() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30000);
        locationRequest.setFastestInterval(10000);
        fusedLocationProviderApi = LocationServices.FusedLocationApi;
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        if (mGoogleApiClient != null) {
            //googleApiClient.connect();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        if(location != null) {
            fusedLocationProviderApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        CURRENT_LOCATION_LAT = location.getLatitude() + "";
        CURRENT_LOCATION_LNG = location.getLongitude() + "";

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(),
                        location.getLongitude()), 13));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        if (mGoogleMap != null)
            getInstitutions();
    }
}
