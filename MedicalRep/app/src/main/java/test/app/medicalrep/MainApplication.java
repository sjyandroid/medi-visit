package test.app.medicalrep;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by Sujay on 21-01-2017.
 */

@ReportsCrashes(
        mailTo = "sujaystestandroid@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_message
)

public class MainApplication extends MultiDexApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        ACRA.init(this);
    }
}
