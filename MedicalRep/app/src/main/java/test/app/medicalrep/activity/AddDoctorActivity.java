package test.app.medicalrep.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.app.medicalrep.R;
import test.app.medicalrep.model.AddDoctorResponse;
import test.app.medicalrep.model.AddInstitutionResponse;
import test.app.medicalrep.rest.ApiClient;
import test.app.medicalrep.rest.ApiInterface;

/**
 * Created by Sujay on 15-01-2017.
 */

public class AddDoctorActivity extends Activity {

    EditText mEdtDocName;
    EditText mEdtDocSpec;
    EditText mEdtDocContact1;
    EditText mEdtDocContact2;
    Button mBtnAdd;

    String inst_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_doctor);

        inst_id = getIntent().getStringExtra("inst_id");

        initUI();

    }

    private void initUI() {
        mEdtDocName = (EditText) findViewById(R.id.edtDocName);
        mEdtDocSpec = (EditText) findViewById(R.id.edtDocSpec);
        mEdtDocContact1 = (EditText) findViewById(R.id.edtDocContact1);
        mEdtDocContact2 = (EditText) findViewById(R.id.edtDocContact2);
        mBtnAdd = (Button) findViewById(R.id.btnAdd);

        mBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateData();
            }
        });

    }

    private void validateData() {
        if(mEdtDocName.getText().toString().trim().length() == 0) {
            Snackbar snackbar = Snackbar.make(mEdtDocName, "Please Enter Doctor Name", 3000);
            snackbar.getView().setBackgroundColor(Color.RED);
            snackbar.show();
            return;
        } else if(mEdtDocSpec.getText().toString().trim().length() == 0) {
            Snackbar.make(mEdtDocSpec, "Please Enter Doctor Specialization", 3000).show();
            return;
        } else if(mEdtDocContact1.getText().toString().trim().length() == 0) {
            Snackbar.make(mEdtDocContact1, "Please Enter Contact 1", 3000).show();
            return;
        } else if(mEdtDocContact2.getText().toString().trim().length() == 0) {
            Snackbar.make(mEdtDocContact2, "Please Enter Contact 2", 3000).show();
            return;
        }

        final ProgressDialog dialog = new ProgressDialog(AddDoctorActivity.this);
        dialog.setMessage("Please Wait");
        dialog.setCancelable(false);
        dialog.show();

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddDoctorResponse> call = apiInterface.addDoctor(inst_id, mEdtDocName.getText().toString().trim(),
                mEdtDocContact1.getText().toString().trim(), mEdtDocContact2.getText().toString().trim(),
                mEdtDocSpec.getText().toString().trim());
        call.enqueue(new Callback<AddDoctorResponse>() {
            @Override
            public void onResponse(Call<AddDoctorResponse> call, Response<AddDoctorResponse> response) {
                dialog.dismiss();
                Toast.makeText(AddDoctorActivity.this,
                        ((AddDoctorResponse)response.body()).getMessage(), Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call<AddDoctorResponse> call, Throwable t) {
                dialog.dismiss();
            }
        });


    }
}
