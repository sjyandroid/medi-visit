package test.app.medicalrep.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.app.medicalrep.MainActivity;
import test.app.medicalrep.R;
import test.app.medicalrep.model.AddInstitutionResponse;
import test.app.medicalrep.rest.ApiClient;
import test.app.medicalrep.rest.ApiInterface;

/**
 * Created by Sujay on 08-01-2017.
 */

public class AddInstitutionActivity extends Activity {

    EditText mEdtInstName;
    EditText mEdtInstAddress;
    ImageView mImgPickPlace;
    EditText mEdtInstContact;
    EditText mEdtInstOffer;
    Spinner mSpinnerInstType;
    Button mBtnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addinstitution);

        initUI();
    }

    void initUI() {
        mEdtInstName = (EditText) findViewById(R.id.edtInstName);
        mEdtInstAddress = (EditText) findViewById(R.id.edtInstAddress);
        mImgPickPlace = (ImageView) findViewById(R.id.imgPickPlace);
        mEdtInstContact = (EditText) findViewById(R.id.edtInstContact);
        mEdtInstOffer = (EditText) findViewById(R.id.edtInstOffer);
        mSpinnerInstType = (Spinner) findViewById(R.id.spinnerInstType);
        mBtnAdd = (Button) findViewById(R.id.btnAdd);

        mBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateData();
            }
        });

        mImgPickPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(intentBuilder.build(AddInstitutionActivity.this), 2);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    String lat, lng;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 2 && data != null) {
            Place place = PlacePicker.getPlace(this, data);
            mEdtInstName.setText(place.getName());
            mEdtInstAddress.setText(place.getAddress());
            mEdtInstContact.setText(place.getPhoneNumber());
            LatLng latLng = place.getLatLng();
            lat = latLng.latitude + "";
            lng = latLng.longitude + "";
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void validateData() {
        if(mEdtInstName.getText().toString().trim().length() == 0) {
            Snackbar.make(mEdtInstName, "Please Enter Institution Name", 3000).show();
            return;
        } else if(mEdtInstAddress.getText().toString().trim().length() == 0) {
            Snackbar.make(mEdtInstName, "Please Enter Institution Address", 3000).show();
            return;
        } else if(mEdtInstContact.getText().toString().trim().length() == 0) {
            Snackbar.make(mEdtInstName, "Please Enter Institution Contact", 3000).show();
            return;
        } else if(mEdtInstOffer.getText().toString().trim().length() == 0) {
            Snackbar.make(mEdtInstName, "Please Enter Institution Offer", 3000).show();
            return;
        } else if(mSpinnerInstType.getSelectedItemPosition() == 0) {
            Snackbar.make(mEdtInstName, "Please Select Institution Type", 3000).show();
            return;
        }

        final ProgressDialog dialog = new ProgressDialog(AddInstitutionActivity.this);
        dialog.setMessage("Please Wait");
        dialog.setCancelable(false);
        dialog.show();

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddInstitutionResponse> call = apiInterface.addInstitution(mEdtInstName.getText().toString().trim(), mEdtInstAddress.getText().toString().trim(),
                mEdtInstContact.getText().toString().trim(), mEdtInstOffer.getText().toString().trim(),
                mSpinnerInstType.getSelectedItem().toString(),
                MainActivity.CURRENT_LOCATION_LAT, MainActivity.CURRENT_LOCATION_LNG);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                dialog.dismiss();
                Toast.makeText(AddInstitutionActivity.this,
                        ((AddInstitutionResponse)response.body()).getMessage(), Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                dialog.dismiss();
            }
        });


    }

}
