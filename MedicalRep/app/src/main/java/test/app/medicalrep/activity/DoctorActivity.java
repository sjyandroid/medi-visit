package test.app.medicalrep.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.app.medicalrep.R;
import test.app.medicalrep.adapter.ProductVisitAdapter;
import test.app.medicalrep.model.Doctor;
import test.app.medicalrep.model.InstDoctor;
import test.app.medicalrep.model.InstDoctorProducts;
import test.app.medicalrep.model.Product;
import test.app.medicalrep.model.ProductVisit;
import test.app.medicalrep.model.SubmitVisitResponse;
import test.app.medicalrep.rest.ApiClient;
import test.app.medicalrep.rest.ApiInterface;

/**
 * Created by Sujay on 15-01-2017.
 */

public class DoctorActivity extends Activity implements ProductVisitAdapter.ProductDeleteCallBack {

    TextView mTxtDocName;
    TextView mTxtDocSpecialization;
    TextView mTxtDocContact;
    TextView mTxtAddProduct;
    TextView mTxtLastVisits;
    RecyclerView mRecyclerProducts;
    RelativeLayout mRelOfferSubmitLayout;
    EditText mEdtOffer;
    Button mBtnSubmitVisit;
    LinearLayoutManager linearLayoutManager;
    ArrayList<Product> mArrayProductsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);

        mTxtDocName = (TextView) findViewById(R.id.txtDocName);
        mTxtDocSpecialization = (TextView) findViewById(R.id.txtDocSpecialization);
        mTxtDocContact = (TextView) findViewById(R.id.txtDocContact);
        mTxtAddProduct = (TextView) findViewById(R.id.txtAddProduct);
        mTxtLastVisits = (TextView) findViewById(R.id.txtLastVisits);
        mBtnSubmitVisit = (Button) findViewById(R.id.btnSubmitVisit);
        mRelOfferSubmitLayout = (RelativeLayout) findViewById(R.id.relOfferSubmitLayout);
        mEdtOffer = (EditText) findViewById(R.id.edtOffer);

        mArrayProductsList = new ArrayList<>();

        mTxtAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(DoctorActivity.this, SearchProductActivity.class), 5);
            }
        });

        mRecyclerProducts = (RecyclerView) findViewById(R.id.recyclerProducts);
        linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerProducts.setLayoutManager(linearLayoutManager);

        getDoctorDetails();

        mBtnSubmitVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //submitVisit();
                showSubmitAlert();
            }
        });

        mTxtLastVisits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, LastVisitsActivity.class);
                intent.putExtra("tar_id", doctorId);
                startActivity(intent);
            }
        });

    }

    void submitVisit(String offer, String comments) {

            ArrayList<ProductVisit> arrayProductVisit = new ArrayList<>();

            JSONArray jsonArray = new JSONArray();

            for(int i = 0; i < mArrayProductsList.size(); i++) {
                ProductVisit productVisit = new ProductVisit();
                productVisit.setPdtId(mArrayProductsList.get(i).getId());
                productVisit.setTarId(doctorId);
                productVisit.setCount(mArrayProductsList.get(i).getQuantity() + "");
                productVisit.setPdt_name(mArrayProductsList.get(i).getPdtName());
                productVisit.setPdt_details(mArrayProductsList.get(i).getPdtDetails());
                productVisit.setPdt_price(mArrayProductsList.get(i).getPdtPrice());
                arrayProductVisit.add(productVisit);
                jsonArray.put(productVisit.getJSONObject());
            }

            String empId = getSharedPreferences("app_prefs", MODE_PRIVATE).getString("userID", "");
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

            //JSONArray array = new JSONArray(arrayProductVisit);

            System.out.println(jsonArray.toString());

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SubmitVisitResponse> call = apiInterface.submitVisit(
                    empId,
                    empId, inst_id, doctorId,
                    "lat", "lng", "dist", format.format(calendar.getTime()), comments,
                    offer, jsonArray.toString());
            call.enqueue(new Callback<SubmitVisitResponse>() {
                @Override
                public void onResponse(Call<SubmitVisitResponse> call, Response<SubmitVisitResponse> response) {
                    if(response != null && response.body() != null) {
                        if(response.body().getError().equals("0")) {
                            Toast.makeText(DoctorActivity.this, response.body().getMessage(),
                                    Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(DoctorActivity.this, "Unknown error. Please try after some time",
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(DoctorActivity.this, "UNAUTHORIZED USER", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<SubmitVisitResponse> call, Throwable t) {

                }
            });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 5) {
            if(resultCode == RESULT_OK) {
                String id = data.getStringExtra("_id");
                String pdt_details = data.getStringExtra("pdt_details");
                String pdt_price = data.getStringExtra("pdt_price");
                String pdt_type = data.getStringExtra("pdt_type");
                String comp_id = data.getStringExtra("comp_id");
                String pdt_name = data.getStringExtra("pdt_name");
                String comp_name = data.getStringExtra("comp_name");
                Product product = new Product();
                product.setId(id);
                product.setPdtDetails(pdt_details);
                product.setPdtPrice(pdt_price);
                product.setPdtType(pdt_type);
                product.setCompId(comp_id);
                product.setPdtName(pdt_name);
                product.setCompName(comp_name);
                mArrayProductsList.add(product);
                mRecyclerProducts.setAdapter(new ProductVisitAdapter(DoctorActivity.this,
                        mArrayProductsList));
                if(mArrayProductsList.size() > 0) {
                    mRelOfferSubmitLayout.setVisibility(View.VISIBLE);
                } else {
                    mRelOfferSubmitLayout.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    String doctorId;
    String inst_id;
    ArrayList<InstDoctorProducts> doctorProducts;
    private void getDoctorDetails() {
        doctorId = getIntent().getStringExtra("id");
        String name = getIntent().getStringExtra("name");
        inst_id = getIntent().getStringExtra("inst_id");
        String specialization = getIntent().getStringExtra("specialization");
        String contact = getIntent().getStringExtra("contact1")
                + ", " + getIntent().getStringExtra("contact2");

        doctorProducts = (ArrayList<InstDoctorProducts>) getIntent().getSerializableExtra("products");

        mTxtDocName.setText(name);
        mTxtDocSpecialization.setText(specialization);
        mTxtDocContact.setText(contact);

        for(int i = 0; i < doctorProducts.size(); i++) {
            Product product = new Product();
            product.setId(doctorProducts.get(i).getPdt_id());
            product.setQuantity(Integer.parseInt(doctorProducts.get(i).getCount()));
            product.setPdtName(doctorProducts.get(i).getPdt_name());
            product.setPdtDetails(doctorProducts.get(i).getPdt_details());
            product.setPdtPrice(doctorProducts.get(i).getPdt_price());
            mArrayProductsList.add(product);
        }

        mRecyclerProducts.setAdapter(new ProductVisitAdapter(DoctorActivity.this,
                mArrayProductsList));
        if(mArrayProductsList.size() > 0) {
            mRelOfferSubmitLayout.setVisibility(View.VISIBLE);
        } else {
            mRelOfferSubmitLayout.setVisibility(View.INVISIBLE);
        }

    }

    void showSubmitAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(DoctorActivity.this);
        builder.setCancelable(true);
        builder.setTitle("Submit Visit");

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_submit_visit, null);
        builder.setView(dialogView);

        final EditText edtOffer = (EditText) dialogView.findViewById(R.id.edtOffer);
        final EditText edtComments = (EditText) dialogView.findViewById(R.id.edtComments);

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(edtOffer.getText().toString().trim().length() == 0) {
                    Toast.makeText(DoctorActivity.this, "Please Enter Offer", Toast.LENGTH_LONG).show();
                } else if(edtComments.getText().toString().trim().length() == 0) {
                    Toast.makeText(DoctorActivity.this, "Please Enter Comments", Toast.LENGTH_LONG).show();
                } else {
                    submitVisit(edtOffer.getText().toString().trim(), edtComments.getText().toString().trim());
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();


    }

    @Override
    public void deletedPosition(int position) {
        mArrayProductsList.remove(position);
        mRecyclerProducts.setAdapter(new ProductVisitAdapter(DoctorActivity.this,
                mArrayProductsList));
        if(mArrayProductsList.size() > 0) {
            mRelOfferSubmitLayout.setVisibility(View.VISIBLE);
        } else {
            mRelOfferSubmitLayout.setVisibility(View.INVISIBLE);
            //mEdtOffer.setText("");
        }
    }
}
