package test.app.medicalrep.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.app.medicalrep.R;
import test.app.medicalrep.adapter.EmployeeListAdapter;
import test.app.medicalrep.model.Employee;
import test.app.medicalrep.model.EmployeeResponse;
import test.app.medicalrep.rest.ApiClient;
import test.app.medicalrep.rest.ApiInterface;

/**
 * Created by Sujay on 03-01-2017.
 */

public class EmployeeListActivity extends Activity {

    EditText mEdtSearchEmployees;
    RecyclerView mRecyclerEmployeesList;
    ArrayList<Employee> mArrayEmployeeList;
    ArrayList<Employee> mArrayEmployeeSearchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employees);

        mEdtSearchEmployees = (EditText) findViewById(R.id.edtSearchEmployees);
        mRecyclerEmployeesList = (RecyclerView) findViewById(R.id.recyclerEmployeesList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerEmployeesList.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                mRecyclerEmployeesList.getContext(), linearLayoutManager.getOrientation());
        mRecyclerEmployeesList.addItemDecoration(dividerItemDecoration);

        getEmployees();

        mEdtSearchEmployees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //mArrayEmployeeSearchList = mArrayEmployeeList;
                charSequence = charSequence.toString().trim();

                if(mArrayEmployeeSearchList == null)
                    mArrayEmployeeSearchList = new ArrayList<Employee>();
                else
                    mArrayEmployeeSearchList.clear();

                if(charSequence.toString().trim().length() > 0) {
                    for(int j = 0; j < mArrayEmployeeList.size(); j++) {
                        if(mArrayEmployeeList.get(j).getName().toLowerCase().contains(charSequence.toString().toLowerCase())){
                            mArrayEmployeeSearchList.add(mArrayEmployeeList.get(j));
                        }
                    }
                    mRecyclerEmployeesList.setAdapter(new EmployeeListAdapter(EmployeeListActivity.this,
                            mArrayEmployeeSearchList));
                } else {
                    mRecyclerEmployeesList.setAdapter(new EmployeeListAdapter(EmployeeListActivity.this,
                            mArrayEmployeeList));
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    void getEmployees() {

        final ProgressDialog dialog = new ProgressDialog(EmployeeListActivity.this);
        dialog.setMessage("Please Wait");
        dialog.setCancelable(false);
        dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<EmployeeResponse> call = apiService.getEmployees();
        call.enqueue(new Callback<EmployeeResponse>() {
            @Override
            public void onResponse(Call<EmployeeResponse> call, Response<EmployeeResponse> response) {
                dialog.dismiss();
                mArrayEmployeeList = response.body().getEmployees();
                mRecyclerEmployeesList.setAdapter(new EmployeeListAdapter(
                        EmployeeListActivity.this, mArrayEmployeeList));

            }

            @Override
            public void onFailure(Call<EmployeeResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(EmployeeListActivity.this, "Unknown error. Please try after some time", Toast.LENGTH_LONG).show();
            }
        });


        /*mArrayEmployeeList = new ArrayList<Employee>();
        mArrayEmployeeList.add(new Employee( "Donald Baits", "101", ""));mArrayEmployeeList.add(new Employee( "Kenny Brenin", "102", ""));
        mArrayEmployeeList.add(new Employee( "Kyle Burr", "103", ""));mArrayEmployeeList.add(new Employee( "Arnie Cade", "104", ""));
        mArrayEmployeeList.add(new Employee( "Bud Cann", "105", ""));mArrayEmployeeList.add(new Employee( "Eric Carwin", "106", ""));
        mArrayEmployeeList.add(new Employee( "Jimmy Darrok", "107", ""));mArrayEmployeeList.add(new Employee( "John Doe", "108", ""));
        mArrayEmployeeList.add(new Employee( "Hommer Fales", "109", ""));mArrayEmployeeList.add(new Employee( "Lisa Fane", "110", ""));
        mArrayEmployeeList.add(new Employee( "Jeff Fischer", "111", ""));mArrayEmployeeList.add(new Employee( "Clark Hallam", "112", ""));
        mArrayEmployeeList.add(new Employee( "Luke Keen", "113", ""));mArrayEmployeeList.add(new Employee( "Harry Nevin", "114", ""));
*/
        /*mArrayEmployeeList.add("");mArrayEmployeeList.add("");mArrayEmployeeList.add("");
        mArrayEmployeeList.add("");mArrayEmployeeList.add("");mArrayEmployeeList.add("");
        mArrayEmployeeList.add("");mArrayEmployeeList.add("");mArrayEmployeeList.add("");
        mArrayEmployeeList.add("");mArrayEmployeeList.add("");mArrayEmployeeList.add("");
        mArrayEmployeeList.add("");mArrayEmployeeList.add("");mArrayEmployeeList.add("");
        mArrayEmployeeList.add("");mArrayEmployeeList.add("");mArrayEmployeeList.add("");*/
    }
}
