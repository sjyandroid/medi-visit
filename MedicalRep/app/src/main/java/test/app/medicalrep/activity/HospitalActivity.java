package test.app.medicalrep.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.app.medicalrep.R;
import test.app.medicalrep.adapter.DoctorsListAdapter;
import test.app.medicalrep.model.InstDoctor;
import test.app.medicalrep.model.InstDoctorResponse;
import test.app.medicalrep.rest.ApiClient;
import test.app.medicalrep.rest.ApiInterface;

/**
 * Created by Sujay on 04-01-2017.
 */

public class HospitalActivity extends Activity {

    TextView mTxtInstName;
    TextView mTxtInstAddress;
    TextView mTxtInstContact;
    TextView mBtnAddDoctor;
    RecyclerView mRecyclerDoctors;
    LinearLayoutManager linearLayoutManager;
    ArrayList<InstDoctor> mArrayDoctorsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital);

        mTxtInstName = (TextView) findViewById(R.id.txtInstName);
        mTxtInstAddress = (TextView) findViewById(R.id.txtInstAddress);
        mTxtInstContact = (TextView) findViewById(R.id.txtInstContact);
        mBtnAddDoctor = (TextView) findViewById(R.id.btnAddDoctor);

        mBtnAddDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HospitalActivity.this, AddDoctorActivity.class);
                intent.putExtra("inst_id", id);
                startActivity(intent);
            }
        });



        mRecyclerDoctors = (RecyclerView) findViewById(R.id.recyclerDoctors);
        linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerDoctors.setLayoutManager(linearLayoutManager);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getHospitalDetails();
    }

    String id;
    private void getHospitalDetails() {
        String inst_contact = getIntent().getStringExtra("inst_contact");
        String inst_addr = getIntent().getStringExtra("inst_addr");
        String inst_offer = getIntent().getStringExtra("inst_offer");
        String inst_type = getIntent().getStringExtra("inst_type");
        String inst_lng = getIntent().getStringExtra("inst_lng");
        String inst_lat = getIntent().getStringExtra("inst_lat");
        String inst_name = getIntent().getStringExtra("inst_name");
        id = getIntent().getStringExtra("_id");
        String v = getIntent().getStringExtra("__v");

        mTxtInstName.setText(inst_name);
        mTxtInstAddress.setText(inst_addr);
        mTxtInstContact.setText(inst_contact);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<InstDoctorResponse> call = apiInterface.getDoctors("target/list_targets_inst/"+id);
        call.enqueue(new Callback<InstDoctorResponse>() {
            @Override
            public void onResponse(Call<InstDoctorResponse> call, Response<InstDoctorResponse> response) {
                if(response.body().getError().equals("0")) {
                    mArrayDoctorsList = response.body().getTargets();
                    mRecyclerDoctors.setAdapter(new DoctorsListAdapter(HospitalActivity.this,mArrayDoctorsList));
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                            mRecyclerDoctors.getContext(), linearLayoutManager.getOrientation());
                    mRecyclerDoctors.addItemDecoration(dividerItemDecoration);
                } else {
                    Snackbar.make(mRecyclerDoctors, "No Doctor's Available", 3000).show();
                }
            }

            @Override
            public void onFailure(Call<InstDoctorResponse> call, Throwable t) {
                Snackbar.make(mRecyclerDoctors, "Unknown error", 3000).show();
            }
        });

    }
}
