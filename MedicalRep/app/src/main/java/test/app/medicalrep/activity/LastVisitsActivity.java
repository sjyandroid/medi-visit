package test.app.medicalrep.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.app.medicalrep.R;
import test.app.medicalrep.adapter.LastVisitsAdapter;
import test.app.medicalrep.model.Visits;
import test.app.medicalrep.model.VisitsResponse;
import test.app.medicalrep.rest.ApiClient;
import test.app.medicalrep.rest.ApiInterface;

/**
 * Created by sujaysMac on 29/01/17.
 */

public class LastVisitsActivity extends Activity {

    RecyclerView mRecyclerLastVisits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_visits);

        mRecyclerLastVisits = (RecyclerView) findViewById(R.id.recyclerLastVisits);
        mRecyclerLastVisits.setLayoutManager(new LinearLayoutManager(this));

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<VisitsResponse> call = apiInterface.getLastVisits(
                getSharedPreferences("app_prefs", MODE_PRIVATE).getString("userID", ""),
                getIntent().getStringExtra("tar_id"), "5");
        call.enqueue(new Callback<VisitsResponse>() {
            @Override
            public void onResponse(Call<VisitsResponse> call, Response<VisitsResponse> response) {
                if(response.body() != null) {
                    if(response.body().getVisits() != null)
                        mRecyclerLastVisits.setAdapter(new LastVisitsAdapter(response.body().getVisits()));
                    else
                        Snackbar.make(mRecyclerLastVisits, "No Previous Visits", 3000).show();
                }
            }

            @Override
            public void onFailure(Call<VisitsResponse> call, Throwable t) {

            }
        });


    }

}
