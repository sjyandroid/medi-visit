package test.app.medicalrep.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.ArrayList;

import test.app.medicalrep.R;
import test.app.medicalrep.adapter.SearchInstitutionAdapter;
import test.app.medicalrep.model.Institution;

/**
 * Created by Sujay on 17-01-2017.
 */

public class SearchInstitutionActivity extends Activity {

    EditText mEdtSearchInstitution;
    RecyclerView mRecyclerInstitutionList;
    ArrayList<Institution> mArrayInstitutionList;
    ArrayList<Institution> mArrayInstitutionSearchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_institution);

        mEdtSearchInstitution = (EditText) findViewById(R.id.edtSearchInstitution);
        mRecyclerInstitutionList = (RecyclerView) findViewById(R.id.recyclerInstitutionList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerInstitutionList.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                mRecyclerInstitutionList.getContext(), linearLayoutManager.getOrientation());
        mRecyclerInstitutionList.addItemDecoration(dividerItemDecoration);

        mArrayInstitutionList = (ArrayList<Institution>) getIntent().getSerializableExtra("institutions");

        mEdtSearchInstitution.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //mArrayEmployeeSearchList = mArrayEmployeeList;
                charSequence = charSequence.toString().trim();

                if(mArrayInstitutionSearchList == null)
                    mArrayInstitutionSearchList = new ArrayList<Institution>();
                else
                    mArrayInstitutionSearchList.clear();

                if(charSequence.toString().trim().length() > 0) {
                    for(int j = 0; j < mArrayInstitutionList.size(); j++) {
                        if(mArrayInstitutionList.get(j).getInstName().toLowerCase().contains(charSequence.toString().toLowerCase())){
                            mArrayInstitutionSearchList.add(mArrayInstitutionList.get(j));
                        }
                    }
                    mRecyclerInstitutionList.setAdapter(new SearchInstitutionAdapter(SearchInstitutionActivity.this,
                            mArrayInstitutionSearchList));
                } else {
                    mRecyclerInstitutionList.setAdapter(new SearchInstitutionAdapter(SearchInstitutionActivity.this,
                            mArrayInstitutionList));
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
}
