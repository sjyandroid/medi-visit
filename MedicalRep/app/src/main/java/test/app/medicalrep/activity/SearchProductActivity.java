package test.app.medicalrep.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.app.medicalrep.R;
import test.app.medicalrep.adapter.ProductListAdapter;
import test.app.medicalrep.model.Product;
import test.app.medicalrep.model.ProductSearchResponse;
import test.app.medicalrep.rest.ApiClient;
import test.app.medicalrep.rest.ApiInterface;

/**
 * Created by Sujay on 15-01-2017.
 */

public class SearchProductActivity extends Activity {

    EditText mEdtSearchEmployees;
    Button mBtnSearch;
    RecyclerView mRecyclerProducts;

    ArrayList<Product> mArrayProductSearchResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);

        mEdtSearchEmployees = (EditText) findViewById(R.id.edtSearchEmployees);
        mBtnSearch          = (Button) findViewById(R.id.btnSearch);
        mRecyclerProducts   = (RecyclerView) findViewById(R.id.recyclerProducts);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerProducts.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                mRecyclerProducts.getContext(), linearLayoutManager.getOrientation());
        mRecyclerProducts.addItemDecoration(dividerItemDecoration);

        mBtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchProduct();
            }
        });

    }

    void searchProduct() {
        mArrayProductSearchResults = new ArrayList<>();
        
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProductSearchResponse> call = apiInterface.searchProduct("product/search_products/" + mEdtSearchEmployees.getText().toString().trim());
        call.enqueue(new Callback<ProductSearchResponse>() {
            @Override
            public void onResponse(Call<ProductSearchResponse> call, Response<ProductSearchResponse> response) {
                if(!response.body().getError().equals("503")) {
                    mArrayProductSearchResults = response.body().getProducts();
                    mRecyclerProducts.setAdapter(new ProductListAdapter(SearchProductActivity.this,
                            mArrayProductSearchResults));
                } else {
                    mArrayProductSearchResults.clear();
                    mRecyclerProducts.setAdapter(new ProductListAdapter(SearchProductActivity.this,
                            mArrayProductSearchResults));
                    Snackbar.make(mRecyclerProducts, "No Results", 3000).show();
                }
            }

            @Override
            public void onFailure(Call<ProductSearchResponse> call, Throwable t) {

            }
        });
    }

}
