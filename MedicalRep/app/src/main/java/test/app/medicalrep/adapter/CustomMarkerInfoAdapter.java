package test.app.medicalrep.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import test.app.medicalrep.R;

/**
 * Created by Sujay on 05-01-2017.
 */

public class CustomMarkerInfoAdapter implements GoogleMap.InfoWindowAdapter {

    private View view;

    public CustomMarkerInfoAdapter(Context context) {
        view = ((Activity)context).getLayoutInflater().inflate(R.layout.custom_info_window,
                null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
