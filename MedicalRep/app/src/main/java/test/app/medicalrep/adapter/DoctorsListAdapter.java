package test.app.medicalrep.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import test.app.medicalrep.R;
import test.app.medicalrep.activity.DoctorActivity;
import test.app.medicalrep.model.Doctor;
import test.app.medicalrep.model.InstDoctor;

/**
 * Created by Sujay on 04-01-2017.
 */

public class DoctorsListAdapter extends RecyclerView.Adapter<DoctorsListAdapter.DoctorViewHolder> {

    Context mContext;
    ArrayList<InstDoctor> mArrayDoctorsList;

    public DoctorsListAdapter(Context context, ArrayList<InstDoctor> doctorsList) {
        mContext = context;
        mArrayDoctorsList = doctorsList;
    }

    @Override
    public DoctorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_doctor, parent, false);

        DoctorViewHolder viewHolder = new DoctorViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DoctorViewHolder holder, final int position) {
        holder.mTxtDoctorName.setText(mArrayDoctorsList.get(position).getName());
        holder.mTxtDoctorSpecialization.setText(mArrayDoctorsList.get(position).getSpecialization());

        holder.mLinearDoctorRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DoctorActivity.class);
                intent.putExtra("id", mArrayDoctorsList.get(position).getId());
                intent.putExtra("name", mArrayDoctorsList.get(position).getName());
                intent.putExtra("inst_id", mArrayDoctorsList.get(position).getInstId());
                intent.putExtra("specialization", mArrayDoctorsList.get(position).getSpecialization());
                intent.putExtra("contact1", mArrayDoctorsList.get(position).getConatct1());
                intent.putExtra("contact2", mArrayDoctorsList.get(position).getContact2());
                intent.putExtra("products", mArrayDoctorsList.get(position).getProduct_map());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayDoctorsList.size();
    }

    public static class DoctorViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mLinearDoctorRow;
        TextView mTxtDoctorName;
        TextView mTxtDoctorSpecialization;

        public DoctorViewHolder(View itemView) {
            super(itemView);
            mLinearDoctorRow = (LinearLayout) itemView.findViewById(R.id.linearDoctorRow);
            mTxtDoctorName = (TextView) itemView.findViewById(R.id.txtDoctorName);
            mTxtDoctorSpecialization = (TextView) itemView.findViewById(R.id.txtDoctorSpecialization);
        }
    }
}
