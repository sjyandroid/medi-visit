package test.app.medicalrep.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import test.app.medicalrep.MainActivity;
import test.app.medicalrep.R;
import test.app.medicalrep.model.Employee;

/**
 * Created by Sujay on 03-01-2017.
 */

public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.EmployeeViewHolder> {

    Context mContext;
    ArrayList<Employee> mArrayEmployeesList;

    public EmployeeListAdapter(Context context, ArrayList<Employee> list) {
        mContext = context;
        mArrayEmployeesList = list;
    }

    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_employee, parent, false);

        EmployeeViewHolder vh = new EmployeeViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(EmployeeViewHolder holder, final int position) {
        holder.mTxtEmployeeName.setText(mArrayEmployeesList.get(position).getName());
        holder.mTxtEmployeeId.setText(mArrayEmployeesList.get(position).getId());
        holder.mLinearEmployeeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mArrayEmployeesList.get(position).getStatus().equalsIgnoreCase("ACTIVE")) {
                    mContext.startActivity(new Intent(mContext, MainActivity.class));
                    SharedPreferences prefs = mContext.getSharedPreferences("app_prefs", Context.MODE_PRIVATE);
                    prefs.edit().putString("userID", mArrayEmployeesList.get(position).getId()).commit();
                    ((Activity) mContext).finish();
                } else {
                    new AlertDialog.Builder(mContext)
                        .setTitle("Account Inactive")
                        .setMessage("Please activate your account")
                        .setPositiveButton("Ok", null).show();
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayEmployeesList.size();
    }

    public static class EmployeeViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mLinearEmployeeLayout;
        TextView mTxtEmployeeName;
        TextView mTxtEmployeeId;

        public EmployeeViewHolder(View itemView) {
            super(itemView);
            mLinearEmployeeLayout = (LinearLayout) itemView.findViewById(R.id.linearEmployeeLayout);
            mTxtEmployeeName = (TextView) itemView.findViewById(R.id.txtEmployeeName);
            mTxtEmployeeId = (TextView) itemView.findViewById(R.id.txtEmployeeId);
        }
    }
}
