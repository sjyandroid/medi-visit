package test.app.medicalrep.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import test.app.medicalrep.R;
import test.app.medicalrep.model.InstDoctorProducts;
import test.app.medicalrep.model.Visits;

/**
 * Created by sujaysMac on 29/01/17.
 */

public class LastVisitsAdapter extends RecyclerView.Adapter<LastVisitsAdapter.VisitViewHolder> {

    ArrayList<Visits> mArrayLastVisits;

    public LastVisitsAdapter(ArrayList<Visits> mArrayLastVisits) {
        this.mArrayLastVisits = mArrayLastVisits;
    }

    @Override
    public VisitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_visit, parent, false);
        VisitViewHolder vh = new VisitViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(VisitViewHolder holder, int position) {
        holder.txtVisitDate.setText(mArrayLastVisits.get(position).getEntered_date());
        holder.txtVisitDetails.setText(mArrayLastVisits.get(position).getDetails());
        holder.txtVisitGiftCost.setText(mArrayLastVisits.get(position).getGift_cost());
        ArrayList<InstDoctorProducts> productsList = mArrayLastVisits.get(position).getProducts();
        StringBuilder products = new StringBuilder();
        for (int i = 0; i < productsList.size(); i++) {
            products.append(productsList.get(i).getPdt_name());
            products.append("(" + productsList.get(i).getCount() + ")\n ");
        }
        /*if(products.length() > 0)
            products.deleteCharAt(products.length() -1);*/
        holder.txtVisitProducts.setText(products);
    }

    @Override
    public int getItemCount() {
        return mArrayLastVisits.size();
    }

    public class VisitViewHolder extends RecyclerView.ViewHolder{

        TextView txtVisitDate;
        TextView txtVisitDetails;
        TextView txtVisitGiftCost;
        TextView txtVisitProducts;

        public VisitViewHolder(View itemView) {
            super(itemView);
            txtVisitDate = (TextView) itemView.findViewById(R.id.txtVisitDate);
            txtVisitDetails = (TextView) itemView.findViewById(R.id.txtVisitDetails);
            txtVisitGiftCost = (TextView) itemView.findViewById(R.id.txtVisitGiftCost);
            txtVisitProducts = (TextView) itemView.findViewById(R.id.txtVisitProducts);
        }
    }
}
