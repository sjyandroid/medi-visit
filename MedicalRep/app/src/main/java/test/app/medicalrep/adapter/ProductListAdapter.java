package test.app.medicalrep.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import test.app.medicalrep.R;
import test.app.medicalrep.model.Product;

/**
 * Created by Sujay on 15-01-2017.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductViewHolder> {

    Context mContext;
    ArrayList<Product> mArrayProductsList;

    public ProductListAdapter(Context context, ArrayList<Product> mArrayProductsList) {
        mContext = context;
        this.mArrayProductsList = mArrayProductsList;
    }

    @Override
    public ProductListAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_product, parent, false);

        ProductViewHolder vh = new ProductViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ProductListAdapter.ProductViewHolder holder, final int position) {
        holder.txtMedName.setText(mArrayProductsList.get(position).getPdtName());
        holder.txtMedDetail.setText(mArrayProductsList.get(position).getPdtDetails());
        holder.txtMedPrice.setText(mArrayProductsList.get(position).getPdtPrice());

        holder.cardProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("_id", mArrayProductsList.get(position).getId());
                intent.putExtra("pdt_details", mArrayProductsList.get(position).getPdtDetails());
                intent.putExtra("pdt_price", mArrayProductsList.get(position).getPdtPrice());
                intent.putExtra("pdt_type", mArrayProductsList.get(position).getPdtType());
                intent.putExtra("comp_id", mArrayProductsList.get(position).getCompId());
                intent.putExtra("pdt_name", mArrayProductsList.get(position).getPdtName());
                intent.putExtra("comp_name", mArrayProductsList.get(position).getCompName());

                ((Activity) mContext).setResult(Activity.RESULT_OK, intent);
                ((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayProductsList.size();
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {

        CardView cardProduct;
        TextView txtMedName;
        TextView txtMedDetail;
        TextView txtMedPrice;

        public ProductViewHolder(View itemView) {
            super(itemView);
            cardProduct = (CardView) itemView.findViewById(R.id.cardProduct);
            txtMedName = (TextView) itemView.findViewById(R.id.txtMedName);
            txtMedDetail = (TextView) itemView.findViewById(R.id.txtMedDetail);
            txtMedPrice = (TextView) itemView.findViewById(R.id.txtMedPrice);
        }

    }
}
