package test.app.medicalrep.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import test.app.medicalrep.R;
import test.app.medicalrep.model.Product;

/**
 * Created by Sujay on 15-01-2017.
 */

public class ProductVisitAdapter extends RecyclerView.Adapter<ProductVisitAdapter.ProductVisitViewHolder> {

    Context mContext;
    ArrayList<Product> mArrayProducts;
    ProductDeleteCallBack callBack;

    public interface ProductDeleteCallBack {
        void deletedPosition(int position);
    }

    public  ProductVisitAdapter(Context context, ArrayList<Product> mArrayProducts) {
        mContext = context;
        this.mArrayProducts = mArrayProducts;
        callBack = (ProductDeleteCallBack) mContext;
    }

    @Override
    public ProductVisitAdapter.ProductVisitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.row_add_product, parent, false);
        ProductVisitViewHolder vh = new ProductVisitViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ProductVisitAdapter.ProductVisitViewHolder holder, final int position) {

        holder.txtMedName.setText(mArrayProducts.get(position).getPdtName());
        holder.txtMedDetail.setText(mArrayProducts.get(position).getPdtDetails());
        holder.txtMedPrice.setText(mArrayProducts.get(position).getPdtPrice());
        holder.txtQuantity.setText(mArrayProducts.get(position).getQuantity() + "");

        holder.txtAddQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = mArrayProducts.get(position).getQuantity();
                quantity++;
                holder.txtQuantity.setText(quantity + "");
                mArrayProducts.get(position).setQuantity(quantity);
            }
        });

        holder.txtSubQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = mArrayProducts.get(position).getQuantity();
                if(quantity != 0) {
                    quantity--;
                    holder.txtQuantity.setText(quantity + "");
                    mArrayProducts.get(position).setQuantity(quantity);
                } else {
                    holder.txtQuantity.setText("0");
                    quantity = 0;
                    mArrayProducts.get(position).setQuantity(quantity);
                    //callBack.deletedPosition(position);
                }
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.deletedPosition(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayProducts.size();
    }

    public static class ProductVisitViewHolder extends RecyclerView.ViewHolder {

        TextView txtMedName;
        TextView txtMedDetail;
        TextView txtMedPrice;
        TextView txtDelete;
        TextView txtAddQuantity;
        TextView txtQuantity;
        TextView txtSubQuantity;

        public ProductVisitViewHolder(View itemView) {
            super(itemView);
            txtMedName = (TextView) itemView.findViewById(R.id.txtMedName);
            txtMedDetail = (TextView) itemView.findViewById(R.id.txtMedDetail);
            txtMedPrice = (TextView) itemView.findViewById(R.id.txtMedPrice);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
            txtAddQuantity = (TextView) itemView.findViewById(R.id.txtAddQuantity);
            txtQuantity = (TextView) itemView.findViewById(R.id.txtQuantity);
            txtSubQuantity = (TextView) itemView.findViewById(R.id.txtSubQuantity);
        }

    }
}
