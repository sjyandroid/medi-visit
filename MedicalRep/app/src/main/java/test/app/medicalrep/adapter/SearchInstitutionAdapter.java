package test.app.medicalrep.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import test.app.medicalrep.MainActivity;
import test.app.medicalrep.R;
import test.app.medicalrep.activity.HospitalActivity;
import test.app.medicalrep.model.Institution;

/**
 * Created by Sujay on 18-01-2017.
 */

public class SearchInstitutionAdapter extends RecyclerView.Adapter<SearchInstitutionAdapter.SearchInstitutionViewHolder> {

    Context mContext;
    ArrayList<Institution> mArrayInstitutionList;

    public SearchInstitutionAdapter(Context context, ArrayList<Institution> mArrayInstitutions) {
        mContext = context;
        this.mArrayInstitutionList = mArrayInstitutions;
    }

    @Override
    public SearchInstitutionAdapter.SearchInstitutionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_search_institution, parent, false);
        SearchInstitutionViewHolder vh = new SearchInstitutionViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(SearchInstitutionAdapter.SearchInstitutionViewHolder holder, final int position) {

        holder.txtHospitalName.setText(mArrayInstitutionList.get(position).getInstName());
        holder.txtHospitalAddr.setText(mArrayInstitutionList.get(position).getInstAddr());
        holder.linearEmployeeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, HospitalActivity.class);
                intent.putExtra("inst_contact",
                        mArrayInstitutionList.get(position).getInstContact());
                intent.putExtra("inst_addr",
                        mArrayInstitutionList.get(position).getInstAddr());
                intent.putExtra("inst_offer",
                        mArrayInstitutionList.get(position).getInstOffer());
                intent.putExtra("inst_type",
                        mArrayInstitutionList.get(position).getInstType());
                intent.putExtra("inst_lng",
                        mArrayInstitutionList.get(position).getInstLng());
                intent.putExtra("inst_lat",
                        mArrayInstitutionList.get(position).getInstLat());
                intent.putExtra("inst_name",
                        mArrayInstitutionList.get(position).getInstName());
                intent.putExtra("_id",
                        mArrayInstitutionList.get(position).getId());
                intent.putExtra("__v",
                        mArrayInstitutionList.get(position).getV());

                mContext.startActivity(intent);
                ((Activity)mContext).finish();
            }
        });


    }

    @Override
    public int getItemCount() {
        return mArrayInstitutionList.size();
    }

    public static class SearchInstitutionViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearEmployeeLayout;
        TextView txtHospitalName;
        TextView txtHospitalAddr;

        public SearchInstitutionViewHolder(View itemView) {
            super(itemView);
            linearEmployeeLayout = (LinearLayout) itemView.findViewById(R.id.linearEmployeeLayout);
            txtHospitalName = (TextView) itemView.findViewById(R.id.txtHospitalName);
            txtHospitalAddr = (TextView) itemView.findViewById(R.id.txtHospitalAddr);
        }

    }
}
