package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sujay on 12-01-2017.
 */

public class AddInstitutionResponse {

    @SerializedName("error")
    private String error;

    @SerializedName("message")
    private String message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
