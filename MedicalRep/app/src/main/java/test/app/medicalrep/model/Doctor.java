package test.app.medicalrep.model;

/**
 * Created by Sujay on 04-01-2017.
 */

public class Doctor {

    String name;
    String designation;

    public Doctor(String name, String designation) {
        this.name = name;
        this.designation = designation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }
}
