package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sujay on 04-01-2017.
 */

public class Employee {

    public Employee(String name, String empId, String status, Integer v) {
        this.id = empId;
        this.name = name;
        this.status = status;
        this.v = v;
    }


    @SerializedName("status")
    private String status;

    @SerializedName("name")
    private String name;

    @SerializedName("_id")
    private String id;

    @SerializedName("__v")
    private Integer v;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }


}
