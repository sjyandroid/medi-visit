package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Sujay on 15-01-2017.
 */

public class InstDoctor {

    @SerializedName("specialization")
    private String specialization;

    @SerializedName("contact2")
    private String contact2;

    @SerializedName("conatct1")
    private String conatct1;

    @SerializedName("inst_id")
    private String instId;

    @SerializedName("name")
    private String name;

    @SerializedName("_id")
    private String id;

    @SerializedName("__v")
    private Integer v;

    @SerializedName("product_map")
    private ArrayList<InstDoctorProducts> product_map = null;

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getContact2() {
        return contact2;
    }

    public void setContact2(String contact2) {
        this.contact2 = contact2;
    }

    public String getConatct1() {
        return conatct1;
    }

    public void setConatct1(String conatct1) {
        this.conatct1 = conatct1;
    }

    public String getInstId() {
        return instId;
    }

    public void setInstId(String instId) {
        this.instId = instId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public ArrayList<InstDoctorProducts> getProduct_map() {
        return product_map;
    }

    public void setProduct_map(ArrayList<InstDoctorProducts> product_map) {
        this.product_map = product_map;
    }
}
