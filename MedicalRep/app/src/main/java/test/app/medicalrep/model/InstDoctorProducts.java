package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by sujaysMac on 29/01/17.
 */

public class InstDoctorProducts implements Serializable {

    @SerializedName("pdt_name")
    private String pdt_name;

    @SerializedName("pdt_id")
    private String pdt_id;

    @SerializedName("count")
    private String count;

    @SerializedName("pdt_details")
    private String pdt_details;

    @SerializedName("pdt_price")
    private String pdt_price;

    public String getPdt_name() {
        return pdt_name;
    }

    public void setPdt_name(String pdt_name) {
        this.pdt_name = pdt_name;
    }

    public String getPdt_id() {
        return pdt_id;
    }

    public void setPdt_id(String pdt_id) {
        this.pdt_id = pdt_id;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPdt_details() {
        return pdt_details;
    }

    public void setPdt_details(String pdt_details) {
        this.pdt_details = pdt_details;
    }

    public String getPdt_price() {
        return pdt_price;
    }

    public void setPdt_price(String pdt_price) {
        this.pdt_price = pdt_price;
    }
}
