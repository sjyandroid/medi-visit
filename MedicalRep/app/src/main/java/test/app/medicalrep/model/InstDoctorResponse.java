package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Sujay on 15-01-2017.
 */

public class InstDoctorResponse {

    @SerializedName("error")
    private String error;

    @SerializedName("targets")
    private ArrayList<InstDoctor> targets = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<InstDoctor> getTargets() {
        return targets;
    }

    public void setTargets(ArrayList<InstDoctor> targets) {
        this.targets = targets;
    }

}
