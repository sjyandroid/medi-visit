package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Sujay on 07-01-2017.
 */

public class Institution implements Serializable {

    @SerializedName("inst_contact")
    private String instContact;

    @SerializedName("inst_addr")
    private String instAddr;

    @SerializedName("inst_offer")
    private String instOffer;

    @SerializedName("inst_type")
    private String instType;

    @SerializedName("inst_lng")
    private String instLng;

    @SerializedName("inst_lat")
    private String instLat;

    @SerializedName("inst_name")
    private String instName;

    @SerializedName("_id")
    private String id;

    @SerializedName("__v")
    private Integer v;

    public String getInstContact() {
        return instContact;
    }

    public void setInstContact(String instContact) {
        this.instContact = instContact;
    }

    public String getInstAddr() {
        return instAddr;
    }

    public void setInstAddr(String instAddr) {
        this.instAddr = instAddr;
    }

    public String getInstOffer() {
        return instOffer;
    }

    public void setInstOffer(String instOffer) {
        this.instOffer = instOffer;
    }

    public String getInstType() {
        return instType;
    }

    public void setInstType(String instType) {
        this.instType = instType;
    }

    public String getInstLng() {
        return instLng;
    }

    public void setInstLng(String instLng) {
        this.instLng = instLng;
    }

    public String getInstLat() {
        return instLat;
    }

    public void setInstLat(String instLat) {
        this.instLat = instLat;
    }

    public String getInstName() {
        return instName;
    }

    public void setInstName(String instName) {
        this.instName = instName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }


}
