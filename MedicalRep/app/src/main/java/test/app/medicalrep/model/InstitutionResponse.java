package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Sujay on 07-01-2017.
 */

public class InstitutionResponse {

    @SerializedName("error")
    private String error;

    @SerializedName("institutions")
    private ArrayList<Institution> institutions = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<Institution> getInstitutions() {
        return institutions;
    }

    public void setInstitutions(ArrayList<Institution> institutions) {
        this.institutions = institutions;
    }

}
