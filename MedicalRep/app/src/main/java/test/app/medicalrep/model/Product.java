package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Sujay on 15-01-2017.
 */

public class Product implements Serializable {

    @SerializedName("pdt_details")
    private String pdtDetails;

    @SerializedName("pdt_price")
    private String pdtPrice;

    @SerializedName("pdt_type")
    private String pdtType;

    @SerializedName("comp_id")
    private String compId;

    @SerializedName("pdt_name")
    private String pdtName;

    @SerializedName("_id")
    private String id;

    @SerializedName("__v")
    private Integer v;

    @SerializedName("comp_name")
    private String compName;

    @SerializedName("count")
    private int quantity = 1;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPdtDetails() {
        return pdtDetails;
    }

    public void setPdtDetails(String pdtDetails) {
        this.pdtDetails = pdtDetails;
    }

    public String getPdtPrice() {
        return pdtPrice;
    }

    public void setPdtPrice(String pdtPrice) {
        this.pdtPrice = pdtPrice;
    }

    public String getPdtType() {
        return pdtType;
    }

    public void setPdtType(String pdtType) {
        this.pdtType = pdtType;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getPdtName() {
        return pdtName;
    }

    public void setPdtName(String pdtName) {
        this.pdtName = pdtName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

}
