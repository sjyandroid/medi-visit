package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Sujay on 15-01-2017.
 */

public class ProductSearchResponse {

    @SerializedName("error")
    private String error;

    @SerializedName("products")
    private ArrayList<Product> products = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

}
