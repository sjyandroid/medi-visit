package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sujay on 21-01-2017.
 */

public class ProductVisit {

    @SerializedName("pdt_id")
    private String pdtId;

    @SerializedName("tar_id")
    private String tarId;

    @SerializedName("count")
    private String count;

    @SerializedName("pdt_name")
    private String pdt_name;

    @SerializedName("pdt_details")
    private String pdt_details;

    @SerializedName("pdt_price")
    private String pdt_price;

    public String getPdtId() {
        return pdtId;
    }

    public void setPdtId(String pdtId) {
        this.pdtId = pdtId;
    }

    public String getTarId() {
        return tarId;
    }

    public void setTarId(String tarId) {
        this.tarId = tarId;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPdt_name() {
        return pdt_name;
    }

    public void setPdt_name(String pdt_name) {
        this.pdt_name = pdt_name;
    }

    public String getPdt_details() {
        return pdt_details;
    }

    public void setPdt_details(String pdt_details) {
        this.pdt_details = pdt_details;
    }

    public String getPdt_price() {
        return pdt_price;
    }

    public void setPdt_price(String pdt_price) {
        this.pdt_price = pdt_price;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("pdt_id", pdtId);
            obj.put("tar_id", tarId);
            obj.put("count", count);
            obj.put("pdt_name", pdt_name);
            obj.put("pdt_details", pdt_details);
            obj.put("pdt_price", pdt_price);
        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

}
