package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sujaysMac on 29/01/17.
 */

public class Visits {

    @SerializedName("inst_dist")
    private String inst_dist;

    @SerializedName("gift_cost")
    private String gift_cost;

    @SerializedName("details")
    private String details;

    @SerializedName("date")
    private String date;

    @SerializedName("entered_date")
    private String entered_date;

    @SerializedName("lng")
    private String lng;

    @SerializedName("lat")
    private String lat;

    @SerializedName("tar_id")
    private String tar_id;

    @SerializedName("emp_id")
    private String emp_id;

    @SerializedName("inst_id")
    private String inst_id;

    @SerializedName("_id")
    private String id;

    @SerializedName("__v")
    private String __v;

    @SerializedName("pdt_visit_map")
    private ArrayList<InstDoctorProducts> products;

    public String getInst_dist() {
        return inst_dist;
    }

    public void setInst_dist(String inst_dist) {
        this.inst_dist = inst_dist;
    }

    public String getGift_cost() {
        return gift_cost;
    }

    public void setGift_cost(String gift_cost) {
        this.gift_cost = gift_cost;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEntered_date() {
        return entered_date;
    }

    public void setEntered_date(String entered_date) {
        this.entered_date = entered_date;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getTar_id() {
        return tar_id;
    }

    public void setTar_id(String tar_id) {
        this.tar_id = tar_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getInst_id() {
        return inst_id;
    }

    public void setInst_id(String inst_id) {
        this.inst_id = inst_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public ArrayList<InstDoctorProducts> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<InstDoctorProducts> products) {
        this.products = products;
    }
}
