package test.app.medicalrep.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sujaysMac on 30/01/17.
 */

public class VisitsResponse {

    @SerializedName("error")
    private String error;

    @SerializedName("visits")
    private ArrayList<Visits> visits = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<Visits> getVisits() {
        return visits;
    }

    public void setVisits(ArrayList<Visits> visits) {
        this.visits = visits;
    }
}
