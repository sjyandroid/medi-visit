package test.app.medicalrep.rest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;
import test.app.medicalrep.model.AddDoctorResponse;
import test.app.medicalrep.model.AddInstitutionResponse;
import test.app.medicalrep.model.EmployeeResponse;
import test.app.medicalrep.model.InstDoctorResponse;
import test.app.medicalrep.model.InstitutionResponse;
import test.app.medicalrep.model.ProductSearchResponse;
import test.app.medicalrep.model.ProductVisit;
import test.app.medicalrep.model.SubmitVisitResponse;
import test.app.medicalrep.model.Visits;
import test.app.medicalrep.model.VisitsResponse;

/**
 * Created by Sujay on 07-01-2017.
 */

public interface ApiInterface {

    @GET("employee/list_employee")
    Call<EmployeeResponse> getEmployees();

    @GET("institution/list_institution")
    Call<InstitutionResponse> getInstitutions(@Header("user_id") String user_id);

    @FormUrlEncoded
    @POST("institution/add_institution")
    Call<AddInstitutionResponse> addInstitution(@Field("inst_name") String name,
                                                @Field("inst_addr") String address,
                                                @Field("inst_contact") String contact,
                                                @Field("inst_offer") String offer,
                                                @Field("inst_type") String type,
                                                @Field("inst_lat") String lat,
                                                @Field("inst_lng") String lng);

    @GET
    Call<InstDoctorResponse> getDoctors(@Url String inst_id);

    @FormUrlEncoded
    @POST("target/add_target")
    Call<AddDoctorResponse> addDoctor(@Field("inst_id") String inst_id,
                                      @Field("tar_name") String tar_name,
                                      @Field("tar_contact1") String tar_contact1,
                                      @Field("tar_contact2") String tar_contact2,
                                      @Field("tar_spec") String tar_spec);

    @GET
    Call<ProductSearchResponse> searchProduct(@Url String name);

    @POST("visit/add_visit")
    @FormUrlEncoded
    Call<SubmitVisitResponse> submitVisit(@Header("user_id") String user_id,
                                          @Field("emp_id") String emp_id,
                                          @Field("inst_id") String inst_id,
                                          @Field("tar_id") String tar_id,
                                          @Field("lat") String lat,
                                          @Field("lng") String lng,
                                          @Field("inst_dist") String inst_dist,
                                          @Field("entered_date") String entered_date,
                                          @Field("details") String details,
                                          @Field("gift_cost") String gift_cost,
                                          @Field("pdt_visit_map") String list);

    @POST("visit/lastVisits")
    @FormUrlEncoded
    Call<VisitsResponse> getLastVisits(@Header("user_id") String user_id,
                                       @Field("tar_id") String tar_id,
                                       @Field("limit") String limit);

}
